package com.bullhead.translator.backend;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class TranslatorApi {
    private static final String API_KEY = "trnsl.1.1.20180620T022334Z.0e17b61b9b95c3f5.f36c6f40cf4e1de986933280019fafa3246b8f9d";
    private static final String BASE_URL = "https://translate.yandex.net/api/v1.5/tr.json/translate?" +
            "key=%1s" +
            "&text=%2s" +
            "&lang=%3s" +
            "&format=text";
    private static TranslatorApi instance;

    public static TranslatorApi getInstance() {
        if (instance == null) {
            instance = new TranslatorApi();
        }
        return instance;
    }

    private static String getResponse(String url) throws Exception {
        URL httpUrl = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) httpUrl.openConnection();
        InputStream is = connection.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        line = sb.toString();
        connection.disconnect();
        is.close();
        sb.delete(0, sb.length());
        return line;
    }

    public void translate(@NonNull final String text,
                          @NonNull final String sourceLang,
                          @NonNull final String destLang,
                          @NonNull final OnTranslateDone completion) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String encodedText = URLEncoder.encode(text, "UTF-8");
                    String lang = sourceLang + "-" + destLang;
                    String url = String.format(BASE_URL, API_KEY, encodedText, lang);
                    final String response = getResponse(url);
                    JSONObject rootObject = new JSONObject(response);
                    JSONArray textArray = rootObject.getJSONArray("text");
                    informSuccess(completion, textArray.getString(0));

                } catch (Exception e) {
                    e.printStackTrace();
                    informError(completion);
                }
            }
        }).start();
    }

    private void informError(final OnTranslateDone completion) {
        if (completion == null) {
            return;
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                completion.onError();
            }
        });
    }

    private void informSuccess(final OnTranslateDone completion, final @NonNull String text) {
        if (completion == null) {
            return;
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                completion.onSuccess(text);
            }
        });
    }

}
