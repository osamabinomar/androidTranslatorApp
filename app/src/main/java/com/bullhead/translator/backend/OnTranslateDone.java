package com.bullhead.translator.backend;

public interface OnTranslateDone {
    void onSuccess(String translation);

    void onError();
}
