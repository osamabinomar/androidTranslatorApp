package com.bullhead.translator.ui.main.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerViewAdapter extends FragmentStatePagerAdapter{
    private final String[] titles;
    private final Fragment[] fragments;
    public PagerViewAdapter(FragmentManager fm,
                            String[] titles, Fragment[] fragments) {
        super(fm);
        this.titles = titles;
        this.fragments = fragments;
        if (titles.length!=fragments.length){
            throw new IllegalStateException("Titles and fragments must be of same size");
        }
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
