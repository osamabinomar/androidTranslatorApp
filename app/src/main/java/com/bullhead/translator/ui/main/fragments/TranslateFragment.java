package com.bullhead.translator.ui.main.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.bullhead.translator.R;
import com.bullhead.translator.backend.DbHelper;
import com.bullhead.translator.backend.OnTranslateDone;
import com.bullhead.translator.backend.TranslatorApi;
import com.bullhead.translator.domain.Translation;
import com.bullhead.translator.ui.views.TranslatorProgressDialog;

public class TranslateFragment extends Fragment implements AdapterView.OnItemSelectedListener, OnTranslateDone {
    private TextView tvTranslated;
    private Button btnTranslate;
    private EditText etEnterText;
    private Spinner spFromLanguage;
    private Spinner spToLangauge;

    private Context context;
    private TranslatorProgressDialog progressDialog;

    private String fromLanguage;
    private String toLanguage;
    private DbHelper dbHelper;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        dbHelper = new DbHelper(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_translator, container, false);
        bindViews(view);
        return view;
    }

    private void bindViews(View view) {
        tvTranslated = view.findViewById(R.id.tvTranslated);
        btnTranslate = view.findViewById(R.id.btnTranslate);
        etEnterText = view.findViewById(R.id.etFrom);
        spFromLanguage = view.findViewById(R.id.spFromLanguage);
        spToLangauge = view.findViewById(R.id.spToLanguage);
        spFromLanguage.setOnItemSelectedListener(this);
        spToLangauge.setOnItemSelectedListener(this);

        btnTranslate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (verifyInput()) {
                    progressDialog = new TranslatorProgressDialog(context);
                    progressDialog.show();
                    String text = etEnterText.getText().toString();
                    TranslatorApi.getInstance().translate(text, fromLanguage, toLanguage,
                            TranslateFragment.this);
                }
            }
        });
    }

    private boolean verifyInput() {
        if (TextUtils.isEmpty(etEnterText.getText().toString())) {
            Snackbar.make(etEnterText, R.string.error_input, Snackbar.LENGTH_SHORT).show();
            return false;
        }
        if (fromLanguage == null) {
            Snackbar.make(spFromLanguage, R.string.error_source_lang, Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (toLanguage == null) {
            Snackbar.make(spFromLanguage, R.string.error_destincation_lang, Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (fromLanguage.equals(toLanguage)) {
            Snackbar.make(spFromLanguage, R.string.error_lang, Snackbar.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spFromLanguage:
                if (position == 0) {
                    fromLanguage = null;
                }
                fromLanguage = getLanguage(position);
                break;
            case R.id.spToLanguage:
                if (position == 0) {
                    toLanguage = null;
                }
                toLanguage = getLanguage(position);
                break;
        }
    }

    private String getLanguage(int position) {
        switch (position) {
            case 1:
                return "ar";
            case 2:
                return "de";
            case 3:
                return "en";
            case 4:
                return "tr";
            case 5:
                return "ur";
            default:
                return null;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onSuccess(String translation) {
        tvTranslated.setText(translation);

        Translation translation1 = new Translation();
        translation1.setTime(System.currentTimeMillis());
        translation1.setText(etEnterText.getText().toString());
        translation1.setDestLang(toLanguage);
        translation1.setSourceLang(fromLanguage);
        translation1.setTranslation(translation);
        dbHelper.saveTranslation(translation1);

        progressDialog.dismiss();
    }

    @Override
    public void onError() {
        progressDialog.dismiss();
        Snackbar.make(etEnterText, R.string.error_translation, Snackbar.LENGTH_LONG).show();
    }
}
