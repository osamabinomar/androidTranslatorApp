package com.bullhead.translator.ui.main.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bullhead.translator.BuildConfig;
import com.bullhead.translator.R;

public class AboutUsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_app_layout, container, false);
        TextView tvVersion = view.findViewById(R.id.appVersion);
        tvVersion.setText(getString(R.string.app_version, BuildConfig.VERSION_NAME));
        return view;
    }
}
