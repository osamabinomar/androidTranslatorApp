package com.bullhead.translator.ui.main.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bullhead.translator.R;
import com.bullhead.translator.domain.Translation;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private final List<Translation> translations;
    private final Context context;

    public HistoryAdapter(@NonNull List<Translation> translations, @NonNull Context context) {
        this.translations = translations;
        this.context = context;
    }

    private static String getFormattedTime(long timestamp) {

        long oneDayInMillis = TimeUnit.DAYS.toMillis(1); // 24 * 60 * 60 * 1000;

        long timeDifference = System.currentTimeMillis() - timestamp;

        return timeDifference < oneDayInMillis
                ? DateFormat.format("hh:mm a", timestamp).toString()
                : DateFormat.format("dd MMM - hh:mm a", timestamp).toString();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_translation, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Translation translation = translations.get(position);
        holder.tvId.setText(String.valueOf(translation.getId()));
        holder.tvTranslation.setText(translation.getTranslation());
        holder.tvText.setText(translation.getText());
        holder.tvTime.setText(getFormattedTime(translation.getTime()));
        holder.tvLanguages.setText(context.getString(R.string.format_language, translation.getSourceLang(),
                translation.getDestLang()));
    }

    @Override
    public int getItemCount() {
        return translations.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvId;
        private TextView tvText;
        private TextView tvTranslation;
        private TextView tvLanguages;
        private TextView tvTime;

        public ViewHolder(View itemView) {
            super(itemView);
            tvId = itemView.findViewById(R.id.tvId);
            tvLanguages = itemView.findViewById(R.id.tvLanguages);
            tvText = itemView.findViewById(R.id.tvText);
            tvTranslation = itemView.findViewById(R.id.tvTranslation);
            tvTime = itemView.findViewById(R.id.tvTime);
        }
    }
}
